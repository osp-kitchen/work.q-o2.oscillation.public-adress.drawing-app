
// WARNING:
// the way those path are encoded is PUNK
// this is why those curves looks COOL
// this is also why it makes no sense in term of svg specification
// there is overlayed points, mix of quadratic and cubic bezier, chain reactions, etc.

// thoughts process / where this came from:
// 0. because theme is public address - the interractives functionnalities on the website particularly matters (point of focus)
// 1. first idea was represent a crowd feeling on a website
//      * how our perception of sound in public place is influenced by the presence of others (layer of noise, heat, crowded or intimacy feeling)
//      * how can this be translated to a website (what its specificities?)
// 2. decided to have a shape that manifest the people presences on the website (when you connect you directly "feel" the presence of others)
// 3. decided to make it interactive - through the chat / or through an added buttons, so the audience can makes this shapes varies according to their experience of the live/sounds
//    it's like dancing or reacting to a sound in physical space, there is not only presence but actions/reactions
// 4. the shape should look/feel connected to sounds in design, but we want to avoid the classic waveform approach
//    so we went with a linguistic effect (bouba/kiki effect) as an inspiration, 
//    that speaks about how the human brain like to connect certain shapes to certain sounds, and has some cultural invariance
//    --> pointy shapes make "k" sounds
//    --> rounded shapes make "b" sounds
// 5. conclusion: 
//     a. the shapes number of points is the number of people connected, so it changes with people connecting/disconnecting
//     b. everybody can react to the point and make their point "dance" by switching it's behavior (pointy or rounded)
// 6. because the theme is public address, it then symbolicaly made sense to see this shape as a cable/link/network between the sounds, the artist and the audience
//    so it has 2 ends that does outside of the canvas, and if a lot of people are there it really feels like a dense network, with lot of connection

// console.log(path);
let viewbox = 1000; // a square
let margins = 200;  // points don't go in those margins
let start_moves_number = 1;
let end_moves_number = 1;
let tpoint = [viewbox/2, 0];
let mpoint = [viewbox/2, viewbox/2];
let bpoint = [viewbox/2, viewbox];
let anchor_length = viewbox/2;

// let possible_move_types = ["L"];
// let possible_move_types = ["S"];
// let possible_move_types = ["T"];
let possible_move_types = ["S", "T"];
// let possible_move_types = ["L", "S", "T"];


// UTILITIES
// ------------------------------------------------------------------------

// -- map a value to a new range
function map_value(value, min, max, new_min, new_max){
    return (((value - min) / (max - min)) * (new_max - new_min)) + new_min;
}

// -- create a point in the viewbox with random position
function create_random_position(){

    let [min,max] = [margins, viewbox - margins];
    let end_point = [Math.random(), Math.random()];

    // map it with en exponent away from the center
    // from: 0 to 1 --> -1 to 1 
    end_point = end_point.map(x => map_value(x, 0, 1, -1, 1));
    // e=2 squared (more in the center - but too much)
    // e=1/2 squareroot (more in the border - not good)
    // --> 1.5 'a little more in the center'
    // let exponent = 1.5;
    // end_point = end_point.map(x => Math.abs(Math.pow(Math.abs(x), exponent)) * Math.sign(x));

    end_point = end_point.map(x => map_value(x, -1, 1, min, max));
    end_point = end_point.map(x => Math.round(x));
    return end_point;
}

function pick_random(arr){
    return arr[Math.floor(Math.random()*arr.length)];
}


// MOVES DESIGN
// ------------------------------------------------------------------------

function craft_move(type, end_point, next_end_point){

    // assign end control point according to move type
    let end_control_point = [];
    let values = [];

    switch(type) {
        case "L":
            // no need to pick start & end control point
            values = end_point;
            break;
        case "S":
            // IDENTITY TRICK 1:
            // the end control point is the next point in path order 
            // (we insert at the start to next is previously added)
            // the start control point is the symmetry of the last one
            // by chaining those it creates loops
            // --> because the end control of a point points toward the next point,
            // and the start control of a point points toward the previous point
            end_control_point = next_end_point;
            values = end_control_point.concat(end_point);
            break;
        case "T":
            // IDENTITY TRICK 2:
            // we chain two T moves as one
            // by chaining those it creates explosions, because (from the spec):
            // The control point is a reflection of the control point of the previous curve command. 
            // If the previous command wasn't a quadratic Bézier curve, 
            // the control point is the same as the curve starting point (current point).
            // --> this create a chain reaction where all their control point are reflections
            // until it reaches the first one (a C move - not quadratic), so they're all reflection
            // of the first start point after the first C move.
            // let new_end_point = end_point.map( x => x + 0.1);
            values = end_point.concat(end_point);
            break;
        default:
            console.log("Error: unrecognised move type");
    }

    let move = {
        type: type,
        values: values
    }

    return move;
}

// -- create a new move based on random
function create_random_move(next_end_point){

    // assign random position
    let end_point = create_random_position();

    // assign random move type
    let type = pick_random(possible_move_types);

    // create the values according to type and positions
    let new_move = craft_move(type, end_point, next_end_point);

    return new_move;
}


// CUSTOM NORMALIZE
// ------------------------------------------------------------------------

function custom_normalize(d){

    // NOTE:
    // this is only applied when sending the svg file
    // those transformation correct the drawing but are not saved
    // still they are fundamental to look at the drawing how it is supposed to be

    // deep cloning
    n_d = JSON.parse(JSON.stringify(d));


    if (n_d.length >= 3){

        // the first 'real' move is a C with start anchor at middle and no end anchor
        // when a new point will be added it will reveal its true nature :)
        // because those changes are not saved and it will not be the d[1] anymore 
        
        // a start spike
        if(n_d[1].type != "S" ){
            d1_end_point = n_d[1].values.slice(-2);
            n_d[1] = {
                type: "C",
                values: [tpoint[0], tpoint[1] + anchor_length,  //start control point
                            d1_end_point[0], d1_end_point[1],      //end control point
                            d1_end_point[0], d1_end_point[1]]      //end point
            }
        }

        // a start loop
        else if(n_d[1].type == "S" ){
            d1_values = n_d[1].values;
            n_d[1] = {
                type: "C",
                values: [tpoint[0], tpoint[1] + anchor_length].concat(d1_values)
            }

            //we have only 3 points
            if (n_d.length == 3){
                n_d[1].values[2] = n_d[n_d.length - 1].fake_last_end_point[0];
                n_d[1].values[3] = n_d[n_d.length - 1].fake_last_end_point[1];
            }
        }

        // the last move can sometimes be a S (sometimes) followed by a S (always),
        // creating a loop that's magnetized to the bottom point
        // this unbalanced the structure (as their control point is always in the margin, but not in this case)
        
        // let dlast_end_point = n_d[n_d.length - 2].values.slice(-2);
        // let last_point_end_anchor_point = [dlast_end_point[1], dlast_end_point[0]];
        if(n_d[n_d.length - 2].type == "S"){
            n_d[n_d.length - 2].values[0] = n_d[n_d.length - 1].fake_last_end_point[0];
            n_d[n_d.length - 2].values[1] = n_d[n_d.length - 1].fake_last_end_point[1];
        }

    }

    return n_d;

}


// ADD / REMOVE MOVES
// ------------------------------------------------------------------------

function compute_fake_last_point(d){
    // either when we removed to last real one
    // or added the first one

    // let real_last_move_index = d.length - end_moves_number - 1;
    // let real_last_end_point = d[real_last_move_index].values.slice(-2);
    d[d.length - 1].fake_last_end_point = create_random_position();
    // console.log("recompute fake last point", d[d.length - 1].fake_last_end_point);
    return d;
}

// -- add a new move,
// for when somebody connect to the website
function add_move(d){

    let first_moves = d.slice(0, start_moves_number);
    let main_moves = d.slice(start_moves_number);
        
    let previous_added_move = main_moves[0];
    let previous_added_end_point = previous_added_move.values.slice(-2);

    // create new move
    let new_move = create_random_move(previous_added_end_point);

    // add the move and update svg
    new_d = first_moves.concat([new_move], main_moves);

    if( new_d.length == 3 ){
        new_d = compute_fake_last_point(new_d);
    }

    return new_d;
}

// -- remove the i move,
// for when somebody disconnect of the website
function remove_move(d, i){

    let number_of_moves = d.length - (start_moves_number+end_moves_number);
    let j = start_moves_number + i;

    // splice to remove elem i
    if(i < number_of_moves){

        // if j-1 is S then 
        // we need to adjust its end control point
        // to keep the rules of the drawing intact
        if (typeof d[j-1] === 'undefined') {
            console.log(j-1);
            console.log(d);
        }
        if (d[j-1].type == "S"){
            let new_next_end_point = d[j+1].values.slice(-2);
            d[j-1].values[0] = new_next_end_point[0];
            d[j-1].values[1] = new_next_end_point[1];
        }

        // if we removed the last 'real' moves,
        // compute a new fake_last_end_point
        if(j == d.length - end_moves_number - 1){
            d = compute_fake_last_point(d);
        }

        // remove j
        d.splice(j,1);
    }
    
    return d;
}

function remove_random_move(d){
    let number_of_moves = d.length - (start_moves_number+end_moves_number);
    let i = Math.floor(Math.random()*number_of_moves);
    d = remove_move(d, i);
    return d;
}

function remove_last_move(d){
    let number_of_moves = d.length - (start_moves_number+end_moves_number);
    let i = number_of_moves - 1;
    d = remove_move(d, i);
    return d;
}


// PATH CREATIONS
// ------------------------------------------------------------------------

// as a line with the two ends points and anchors
function init_path(){

    let init_move = {
        type: "M",
        values: tpoint
    }
    let last_move = {
        type: "S",
        values: [bpoint[0], bpoint[1] - anchor_length,    //end control point
                 bpoint[0], bpoint[1]]                   //end point
    }
    let d = [init_move, last_move];

    return d;
}

// -- create a random path of n move (+ start and end)
function random_path(n){
    d = init_path();
    for (let i = 0; i < n; i++) {
        d = add_move(d);
    }
    return d;
}


// EXPORTS
// ------------------------------------------------------------------------

exports.init_path = init_path;
exports.random_path = random_path;

exports.add_move = add_move;
exports.remove_random_move = remove_random_move;
exports.remove_last_move = remove_last_move;
exports.custom_normalize = custom_normalize;