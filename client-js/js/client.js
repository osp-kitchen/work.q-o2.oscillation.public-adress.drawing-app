
// we have to pass the url there if the websocket handling server-side script is not
// on the same domain than the client static file serving
// in fact: it's just precising to whom it should say the client is connected when it arrives there
// https://socket.io/docs/v3/client-initialization/#from-a-different-domain

let socket;
if (typeof app_domain !== 'undefined'){
    socket = io(app_domain);
}
else{
    socket = io();
}

const svg_id = 'sonic-crowd-network__svg';
const number_id = 'number-of-user';
const moves_id = 'number-of-moves';
const animation_duration = 500;


let svg = document.getElementById(svg_id);
let path = svg.getElementsByTagName('path').item(0);
let number = document.getElementById(number_id);
let moves_number = document.getElementById(moves_id);
let is_the_first_time = true;


console.log("socket.io loaded");

// ----- socket handling

socket.on("numberChange", (number_of_connections, number_of_moves) =>{
    number.innerHTML = number_of_connections;
    if (moves_number){
        moves_number.innerHTML = number_of_moves;
    }
});

// ----- update the svg

socket.on("svg", (d) => {

    if (is_the_first_time){
        // just update
        path.setPathData(d);
        is_the_first_time = false;
    }
    else{
        // animation
        svg.classList.add("updating");

        setTimeout(() => {
            path.setPathData(d);
        }, animation_duration / 2);

        setTimeout(() => {
            svg.classList.remove("updating");
        }, animation_duration);
    }
});