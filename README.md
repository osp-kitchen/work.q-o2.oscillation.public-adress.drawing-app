# Q-O2 Oscillation Public Address - Connected drawing app

Draws an svg where the number of moves is mapped to number of people connected to the server.
It is using node.js together with express and socket.io to use websocket (like the interactivity of etherpad) and update the svg drawing on every client live.

This project is linked to the main website: [work.q-o2.oscillation.public-adress.www](https://gitlab.com/osp-kitchen/www.q-o2.oscillation.public-adress).

## test it locally

    node server.js

then connect to `localhost:3000` and duplicate the tab.

the _client_ folder contains a simple page to test it that will be served.

the _client-js_ folder contains the _js_ needed to handle socket on client side on the actual website (so we don't use an iframe or anything but just request the socket-svg-handling script).

## .env to use it through a single js file on another domain

You have to set up a `.env` file with:

```BASH
NODE_PORT="3000"             
#used to run the node server on a particular port

# NODE_SOCKET_SERVER="domain.name"    
# CURRENTLY CHANGE NOTHING !!
# used to set the socket.io() on the client side to point to this domain (tell the client where it should send the socket communication): if testing locally to same page must be empty, if testing locally from 3000 to another port, it must be 'http://localhost:3000', if testing online 'q-o2.osp.kitchen'
# it does not change the hostname wich is always 'localhost' and rerouted to a subdomain by nGinx when deployed

NODE_ALLOWED_CLIENT="domain.name, another.domain.name"    
#list used to allow CORS (requesting the .js script from a website domain(s) to app domain)
#can be a regex: '/oscillation-festival\.be$' (anything that end with that - either www. or not)
```

If this is not precised it will serve on `http://localhost:3000` (SERVER) allowing client on `http://localhost:8000` (CLIENT) to connect.
Meaning you can serve the website on `http://localhost:8000`, and call those script in it which are going to be accessible if you run the node server on the same time on `127.0.0.1:3000`

```HTML
<!-- automatically delivered by the server side of socket.io -->
<script src="https://q-o2.osp.kitchen/socket.io/socket.io.js"></script>

<!-- served as static file by express -->
<script src="https://q-o2.osp.kitchen/js/path-data-polyfill.js"></script>
<script>
    const app_domain = 'https://q-o2.osp.kitchen';
</script>
<script src="https://q-o2.osp.kitchen/js/client.js"></script>
```

The `app_domain` variable tells the `client.js` script where it should connect the sockets.
<!-- we could write dynamically the client-svg.js to add the correct domain name
and the svg id and the number-of-user id (so all those variables are in server.js), and viewbox coordinate (so all the drawing parameter are on this app - and not in the website) **using jade ?**
  https://stackoverflow.com/questions/35758485/access-server-variable-in-client-side-node-js-express
  https://www.npmjs.com/package/express-state#increase-performance-of-unchanging-or-static-data -->


The website HTML need to have this `svg` in it, which is the one being modified dynamically by `client.js`

```HTML
<svg id="sonic-crowd-network__svg" viewbox="0 0 1000 1000" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg">
    <path d=""/>
</svg>
<p id="sonic-crowd-network__infos">
    There are <span id="number-of-user"></span> persons connected
</p>
```

## Todo:

* think about what happen if a new svg socket event is trigered client side while another one is handled (setTimeout)
* think about what happen if script stop suddenly
* clean db then relaunch server

