
using pseudo notation of `d` svg moves

## adding moves

### init

it init with 2 moves, as a **straight line**
```
M [top]             //pen down
S [mid , bottom]    //bottom, end control point in the middle
```

### first move

the first move is added as an exeption in between, as a **simple spike**
```
M [top]                 //pen down
C [mid , rand1, rand1]  //random position, start control point in the middle, no end control point
S [mid , bottom]        //bottom, end control point in the middle
```

### other moves

we add every move in between the `M` and the last added move.
this is necessary for how we does the moves design (using the last added move, which need to be _after_ in the encoding).
so it's **like a stack**.
```
M [top]                //pen down
? [?, ?, rand2]
C [mid, rand1, rand1]  //random position, start control point in the middle, no end control point
S [mid, bottom]        //bottom, end control point in the middle
```

but we want to keep the top of the drawing like the bottom:
a nice curve with the control move in the middle.
so we convert it to a `C`.
```
M [top]                //pen down
C [mid, rand2, rand2]                   
C [mid, rand1, rand1]  //random position, start control point in the middle, no end control point
S [mid, bottom]        //bottom, end control point in the middle
```

also the now third moves, need to have no start control point anymore...
to solve that we make a difference between
(`real_value` and `real_type`, the one generated with the move design) and 
(`value` and `type`, the one currently used) by using a normalizing function after the adding or removing.

Now the **first added move** can in fact be a custom move and be added like every other!
so the structure we end up with is
```
M [top]                //pen down
C [mid, randn, randn]  //random position, start control point in the middle, no end control point
...
? [?, ?, rand2]
? [?, ?, rand1]        
S [mid, bottom]        //bottom, end control point in the middle
```
with the **last added move** (`C`) revealing it's true nature at the next move added on top of the stack.

## removing moves

we remove at random but not the first `M` or the last `S`.
we have to normalise again, in case we removed the first `C`.

there is a problem: 
if we remove a point following a `S` type, then it's not true anymore than the end control point of this `S` move is the next end point.So by adding and removing, we progressively loose the structure of the drawing...
we have to add a check for that in the remove function!

## top & bottom junctions

the consequences of such a system is:
* the junction between the second and third move is always going to be an angle (because the second one has no end control point), this look not super good if it's `C` followed by `S`. (make a semi angular semi smooth junction)


### bottom

the last move can sometimes be a `S` (sometimes) followed by a `S` (always),
creating a loop that's magnetized to the bottom point.
this unbalanced the structure as the end control point of the `S`'s is always in the margin, but not in this case.

to correct that we map it's control point to the mid point (arbitrarely).

## always remove from bottom ?