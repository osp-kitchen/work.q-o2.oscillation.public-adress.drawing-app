const express = require('express')
const app = express()
const httpServer = require('http').Server(app)

require('dotenv').config();
const { Server } = require('socket.io');
const sqlite3 = require('sqlite3').verbose();


// VARIABLES
// ------------------------------------------------------------------------

const hostname = "localhost";
const port = process.env.NODE_PORT || 3000;

const socket_server_name = process.env.NODE_SOCKET_SERVER || "localhost";

let allowed_clients;
if (process.env.NODE_ALLOWED_CLIENT){
    allowed_clients = process.env.NODE_ALLOWED_CLIENT.split(", ");
}
else{
    allowed_clients = "http://localhost:8000";
}


const svgen = require('./drawing-generator');
const db_path = './recording/changes.db';

let number_of_connections = 0;
let number_of_moves = 0;
let prev_number_of_moves = 0;

// as the shape must be the same for everyone it has to be computed server side
// we only send the computed d attribute to the client
let d = svgen.init_path();
// let d = svgen.random_path(70);


// SERVER
// ------------------------------------------------------------------------

// not necessary anymore because we only need the client.js to be served
app.use(express.static('client'))
// actually used for the website
app.use(express.static('client-js'))

const io = new Server(httpServer, {
    // we have to handle CORS as the client is not on the same domain
    // than those script (see client-svg.js)
    // https://socket.io/docs/v3/handling-cors/
    // https://github.com/expressjs/cors#configuration-options
    cors: {
      origin: allowed_clients,
      methods: ["GET"]
    }
});


// PATHS
// ------------------------------------------------------------------------

// taken and edited from path-data-polyfill
// https://github.com/jarek-foksa/path-data-polyfill
// to dump the path-data json as a string that can be used as an svg path d attribute

function dumpsPathData(d){
    let d_str = "";
    
    for (var i = 0, l = d.length; i < l; i += 1) {
      let seg = d[i];
    
      if (i > 0) {
        d_str += " ";
      }
    
      d_str += seg.type;
    
      if (seg.values && seg.values.length > 0) {
        d_str += " " + seg.values.join(" ");
      }
    }
    return d_str;
}


// RECORDINGS
// ------------------------------------------------------------------------

function db_open(){
    let db = new sqlite3.Database(db_path, (err) => {
        if (err) {
          console.error(err.message);
        }
        console.log('  connected database');
    });
    return db;
}

function db_close(db){
    db.close((err) => {
        if (err) {
          return console.error(err.message);
        }
        console.log('  closed database');
        db_is_writing = false;
    });
}

function db_createTable(db){

    let sql = `
        CREATE TABLE IF NOT EXISTS connections (
        id integer PRIMARY KEY,
        datetime text,
        socket text,
        type text,
        total integer,
        d text,
        normal_d text
        );`;

    db.run(sql, function(err) {
        if (err) {
          return console.log(err.message);
        }
        console.log(`  table created`);
    });
}

function db_insert(db, values_list){

    let sql = `
        INSERT INTO connections(datetime, socket, type, total, d, normal_d)
        VALUES(?,?,?,?,?,?)
        `;

    db.run(sql, values_list, function(err) {
      if (err) {
        return console.log(err.message);
      }
      console.log('  record inserted', values_list[3]);
    });
}

function db_init(){

    let values_list = [
        //we create the date here and not when actually pushing in the db
        new Date().toString(),
        "init",
        "reset",
        0,
        dumpsPathData(d),
        dumpsPathData(svgen.custom_normalize(d))
    ];

    let db = db_open();
    db.serialize(() => {
        db_createTable(db);
        db_insert(db, values_list);
    });
    db_close(db);
}


// every 5 sec we empty the queue and push the changes
// (if we are not already writing! - otherwise the writing can overlap)
// we have to make sure they are all serialized
// otherwise if recieving 50 at the same time we can 
// have an insert beginning when the last one is not finished already

let record_queue = [];
let db_is_writing = false;
setInterval(insertChanges, 5000);

function pushState(socket, type){
    // push every change in a queue

    let values_list = [
        //we create the date here and not when actually pushing in the db
        new Date().toString(),
        socket.id,
        type,
        number_of_connections,
        dumpsPathData(d),
        dumpsPathData(svgen.custom_normalize(d))
    ];
    record_queue.push(values_list);
}

function insertChanges(){

    if( record_queue.length !== 0 && ! db_is_writing){

        // empty the list
        // so it can receive new entry while adding
        console.log("emptying queue");
        db_is_writing = true;
        let add_queue = record_queue;
        record_queue = [];

        let db = db_open();
        db.serialize(function(){
            for(var i =0;i<add_queue.length;i++){
                db_insert(db, add_queue[i]);
            }
        });
        db_close(db);
    }
}


// MOUVEMENTS
// ------------------------------------------------------------------------

function regressive_progression(x){
    // this maps non-linearly:
    // 1    -   1
    // 24   -   15
    // 96   -   48
    // 240  -   105
    // it grow faster than log(x) or sqrt(x), 
    // but less than a linear mapping
    return Math.ceil(Math.pow(x, (Math.log(40)/Math.log(120))));
    // return x;
}

function onConnection(io, socket){
    console.log("+1 connected", socket.id);

    // we have to send the drawing once to have it!
    socket.emit("svg", svgen.custom_normalize(d));

    number_of_connections++;
    prev_number_of_moves = number_of_moves;
    number_of_moves = regressive_progression(number_of_connections);
    if(number_of_moves > prev_number_of_moves){
        d = svgen.add_move(d);
        io.emit("svg", svgen.custom_normalize(d));
    }
    io.emit("numberChange", number_of_connections, number_of_moves);

    pushState(socket, "+1");
}

function onDisconnection(io, socket){
    console.log("-1 connected", socket.id);

    number_of_connections--;
    prev_number_of_moves = number_of_moves;
    number_of_moves = regressive_progression(number_of_connections);
    if(number_of_moves < prev_number_of_moves){
        d = svgen.remove_last_move(d);
        io.emit("svg", svgen.custom_normalize(d));
    }
    io.emit("numberChange", number_of_connections, number_of_moves);

    pushState(socket, "-1");
}


// SOCKETS
// ------------------------------------------------------------------------

// trouble keeping the number exact on refresh
// https://github.com/LearnBoost/socket.io/issues/463
// https://stackoverflow.com/questions/10275667/socket-io-connected-user-count
// io.on("connection", function(s){ 
//     connectedCount += 1; 
//     s.on("disconnect", function(){
//       connectedCount -= 1;
//     });
//   });

// special named event
io.on("connection", (socket) => {
    
    // https://stackoverflow.com/questions/10275667/socket-io-connected-user-count
    // note from an answer: 
    // "I found I needed to make sure I had a socket object to get proper counts consistantly"
    if( socket ){
        onConnection(io, socket);
    }

    // special named event
    socket.on("disconnect", () => {
        onDisconnection(io, socket);
    });


    // for testing
    socket.on("add", () => {
        onConnection(io, socket);
    });
    socket.on("remove", () => {
        onDisconnection(io, socket);
    });

});

// CREATE LISTEN
// ------------------------------------------------------------------------

db_init();

setTimeout( ()=>{
    httpServer.listen(port, hostname, () => {
      console.log(`Server running at http://${hostname}:${port}/`);
      console.log('Allowed domains for CORS are', allowed_clients);
    });
}, 3000);